package com.skywander.birdsound.services.network;

/**
 * Created by skywander on 8/10/16.
 */
public class BirdSoundRetrofitService implements BirdSoundService {

    private RestClient.BirdSoundApi mApi;

    public BirdSoundRetrofitService(RestClient pClient) {
        this.mApi = pClient.create(RestClient.BirdSoundApi.class);
    }
}
