package com.skywander.birdsound.app;

import com.skywander.birdsound.BuildConfig;

/**
 * Created by skywander on 8/10/16.
 */
public class Constants {

    public static final String SHARED_PREFERENCE_NAME = BuildConfig.APPLICATION_ID;
    public static final int CACHE_SIZE = 10 * 1024 * 1024;
    public static final String NAME_RETROFIT_SERVICE = "retrofit";
    public static final String NAME_VOLLEY_SERVICE = "volley";

}
