package com.skywander.birdsound.di.components;

import com.skywander.birdsound.common.base.BaseActivity;
import com.skywander.birdsound.di.modules.InteractorModule;
import com.skywander.birdsound.di.modules.PresenterModule;
import com.skywander.birdsound.di.scopes.PerActivity;

import dagger.Subcomponent;

/**
 * Created by skywander on 8/11/16.
 */
@PerActivity
@Subcomponent(
        modules = {
                PresenterModule.class,
                InteractorModule.class
        }
)
public interface ActivityComponent {
    FragmentComponent plus();
    void inject(BaseActivity baseActivity);
}
