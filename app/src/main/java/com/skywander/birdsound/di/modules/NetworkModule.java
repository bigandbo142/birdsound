package com.skywander.birdsound.di.modules;

import android.content.Context;

import com.skywander.birdsound.app.Constants;
import com.skywander.birdsound.services.network.BirdSoundRetrofitService;
import com.skywander.birdsound.services.network.BirdSoundService;
import com.skywander.birdsound.services.network.RestClient;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by skywander on 8/10/16.
 */
@Module
public class NetworkModule {

    @Singleton @Provides RestClient provideRestClient(Context pContext){
        return new RestClient(pContext);
    }

    @Singleton @Provides @Named(Constants.NAME_RETROFIT_SERVICE) BirdSoundService provideBirdSoundService(RestClient pClient){
        return new BirdSoundRetrofitService(pClient);
    }

}
