package com.skywander.birdsound.di.components;

import com.skywander.birdsound.common.base.BaseFragment;
import com.skywander.birdsound.di.scopes.PerFragment;

import dagger.Subcomponent;

/**
 * Created by skywander on 8/11/16.
 */
@PerFragment
@Subcomponent
public interface FragmentComponent {
    void inject(BaseFragment baseFragment);
}
