package com.skywander.birdsound.app;

import android.app.Application;

import com.skywander.birdsound.di.components.AppComponent;
import com.skywander.birdsound.di.components.DaggerAppComponent;
import com.skywander.birdsound.di.modules.AppModule;
import com.skywander.birdsound.di.modules.NetworkModule;

/**
 * Created by skywander on 8/10/16.
 */
public class BirdSoundApplication extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeComponent();
    }

    public AppComponent getAppComponent(){
        return this.mAppComponent;
    }

    private void initializeComponent(){
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
        mAppComponent.inject(this);
    }
}
