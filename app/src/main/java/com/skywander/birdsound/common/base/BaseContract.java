package com.skywander.birdsound.common.base;

/**
 * Created by skywander on 8/11/16.
 */
public class BaseContract {
    public interface View{

    }

    public interface Presenter {

    }

    public interface Interactor {

    }
}
