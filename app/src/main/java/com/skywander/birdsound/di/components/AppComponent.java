package com.skywander.birdsound.di.components;

import com.skywander.birdsound.app.BirdSoundApplication;
import com.skywander.birdsound.di.modules.AppModule;
import com.skywander.birdsound.di.modules.InteractorModule;
import com.skywander.birdsound.di.modules.NetworkModule;
import com.skywander.birdsound.di.modules.PresenterModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by skywander on 8/10/16.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                NetworkModule.class
        }
)
public interface AppComponent {

    void inject(BirdSoundApplication pApp);

    ActivityComponent plus(PresenterModule presenterModule, InteractorModule interactorModule);
}
