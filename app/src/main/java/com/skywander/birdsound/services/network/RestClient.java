package com.skywander.birdsound.services.network;

import android.content.Context;

import com.skywander.birdsound.app.Constants;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by skywander on 8/10/16.
 * For REST over HTTP(S). Holds the client for other services to put interfaces against.
 */

public class RestClient {

    private final Retrofit mClient;

    public RestClient(Context pContext) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(new Cache(pContext.getCacheDir(), Constants.CACHE_SIZE))
                .build();
        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                return response;
            }
        });

        mClient = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Creates an implementation of the API defined by the ApiInterfaceClass
     * @param apiInterfaceClass
     * @param <T>
     * @return
     */
    public <T> T create(Class<T> apiInterfaceClass) {
        return mClient.create(apiInterfaceClass);
    }

    public interface BirdSoundApi {
        
    }
}
