package com.skywander.birdsound.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.skywander.birdsound.app.BirdSoundApplication;
import com.skywander.birdsound.app.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by skywander on 8/10/16.
 */
@Module
public class AppModule {

    private BirdSoundApplication mApplication;

    public AppModule(BirdSoundApplication pApplication) {
        this.mApplication = pApplication;
    }

    @Singleton @Provides BirdSoundApplication provideApplication(){
        return mApplication;
    }

    @Singleton @Provides SharedPreferences provideSharePreferences(BirdSoundApplication pApplication){
        return pApplication.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    @Singleton @Provides Context provideContext(BirdSoundApplication pApplication){
        return pApplication.getApplicationContext();
    }
}
